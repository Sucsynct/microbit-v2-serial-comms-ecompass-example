# BBC micro:bit Serial Comms and LSM303AGR eCompass Example
This is a simple program written in Rust illustrating how to use the micro:bit
v2's serial comms to receive input from a user, and read and transmit data from
the eCompass module.

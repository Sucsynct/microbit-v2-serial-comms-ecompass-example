/*
 * A simple struct which acts as a ring buffer useful for reading/writing using serial
 * communication protocols.
 * Copyright (C) 2023  Sucsynct
 */

use core::fmt::Write;

type Buffer<const N: usize> = [u8; N];

pub struct RingBuffer<const N: usize> {
    pub i: usize,
    pub buffer: Buffer<N>,
}

impl<const N: usize> RingBuffer<N> {
    pub fn new() -> Self {
        RingBuffer {
            i: 0,
            buffer: [0; N],
        }
    }

    pub fn push(&mut self, element: u8) {
        if self.i >= N {
            self.i = 0;
        }

        self.buffer[self.i] = element;
        self.i += 1;
    }

    pub fn flush(&mut self) -> Buffer<N> {
        let out = self.buffer.clone();

        for j in 0..self.i {
            self.buffer[j] = 0;
        }

        self.i = 0;

        out
    }

    pub fn latest_byte(&self) -> u8 {
        self.buffer[self.i]
    }
}

impl<const N: usize> Write for RingBuffer<N> {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.i = 0;

        for byte in s.as_bytes() {
            self.buffer[self.i] = *byte;

            if self.i == N - 1 {
                break;
            }

            self.i += 1;
        }

        Ok(())
    }
}

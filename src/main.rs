/*
 * A simple program which accesses the the LSM303AGR on the BBC micro:bit v2 and transmits data via
 * the board's UART interface.
 * Copyright (C) 2023  Sucsynct
 */

#![no_std]
#![no_main]

use cortex_m_rt::entry;
use microbit::{
    hal::uarte::{
        self,
        Parity,
        Baudrate,
    },
    hal::twim::{
        self,
        Frequency,
    },
};

use rtt_target::rtt_init_print;
use panic_rtt_target as _;
use lsm303agr::{Lsm303agr, AccelOutputDataRate, MagOutputDataRate};
use core::str::from_utf8;
use core::fmt::Write;

mod ring_buffer;
use ring_buffer::RingBuffer;

static mut RX_BUFFER: RingBuffer<1> = RingBuffer { i: 0, buffer: [0; 1] };

#[entry]
fn main() -> ! {
    rtt_init_print!();
    let board = microbit::Board::take().unwrap();

    let mut serial_uarte = uarte::Uarte::new(
        board.UARTE0,
        board.uart.into(),
        Parity::EXCLUDED,
        Baudrate::BAUD115200,
    );

    let serial_i2c = twim::Twim::new(
        board.TWIM0,
        board.i2c_internal.into(),
        Frequency::K100,
    );

    let mut sensor = Lsm303agr::new_with_i2c(serial_i2c);
    sensor.init().unwrap();
    sensor.set_accel_odr(AccelOutputDataRate::Hz10).unwrap();
    sensor.set_mag_odr(MagOutputDataRate::Hz10).unwrap();
    let mut sensor = sensor.into_mag_continuous().ok().unwrap();

    let mut command_buffer = RingBuffer::<15>::new();

    loop {
        serial_uarte.read(unsafe { &mut RX_BUFFER.buffer }).unwrap();
        let byte = unsafe { RX_BUFFER.latest_byte() };
        if byte == b'\r' {
            let command = from_utf8(&command_buffer.buffer[0..command_buffer.i]).unwrap();

            let data;

            match command {
                "accelerometer" => {
                    while !sensor.accel_status().unwrap().xyz_new_data {}
                    data = sensor.accel_data().unwrap();
                    write!(serial_uarte,
                        "Accel Data | x: {}, y: {}, z: {}\n\r",
                        data.x,
                        data.y,
                        data.z).unwrap();
                },

                "magnetometer" => {
                    while !sensor.mag_status().unwrap().xyz_new_data {}
                    data = sensor.mag_data().unwrap();
                    write!(serial_uarte, "Mag Data | x: {}, y: {}, z: {}\n\r",
                        data.x,
                        data.y,
                        data.z).unwrap();
                },
                _ => {
                    write!(serial_uarte,
                        "Please enter either \"accelerometer\" or \"magnetometer\"\n\r").unwrap();
                }
            }

            command_buffer.flush();
        } else {
            command_buffer.push(byte);
        }
    }
}
